# -------------------------------------------------
# Project created by QtCreator 2009-10-09T09:55:02
# -------------------------------------------------
QT += webkit
TARGET = WebScraps
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    webview.cpp \
    webscrap.cpp \
    graphicsview.cpp \
    graphicstoolbar.cpp \
    resizeuihelper.cpp \
    searchlineedit.cpp \
    addressbar.cpp
HEADERS += mainwindow.h \
    webview.h \
    webscrap.h \
    graphicsview.h \
    graphicstoolbar.h \
    resizeuihelper.h \
    searchlineedit.h \
    addressbar.h
FORMS += mainwindow.ui
RESOURCES += webscraps.qrc

RCC_DIR     = $$PWD/.rcc
UI_DIR      = $$PWD/.ui
MOC_DIR     = $$PWD/.moc
OBJECTS_DIR = $$PWD/.obj

