/** This file is part of WebScraps **
 *
 * Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).*
 * All rights reserved.

 * Contact:  Nokia Corporation (qt-info@nokia.com)**

 * You may use this file under the terms of the BSD license as follows:
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice, this list
 *      of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice, this
 *      list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution.
 *   3. Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the names of 
 *      its contributors may be used to endorse or promote products derived from this software 
 *      without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QPainter>
#include <QGraphicsProxyWidget>
#include <QGraphicsRotation>
#include <QList>
#include <QGraphicsTransform>
#include <QPropertyAnimation>
#include <QParallelAnimationGroup>
#include <QState>
#include <QEventTransition>
#include <QGraphicsSceneHoverEvent>
#include "graphicstoolbar.h"

bool HoverOutEventTransition::eventTest(QEvent *e)
{
    if (! QEventTransition::eventTest(e))
        return false;
    QGraphicsWidget *toolbar = qobject_cast<QGraphicsWidget*>(eventSource());
    // if focus is now on a widget contained within the toolbar, don't transition
    if (toolbar && toolbar->scene() && toolbar->isAncestorOf(toolbar->scene()->focusItem()))
        return false;
    return true;
}

GraphicsToolBar::GraphicsToolBar(QGraphicsScene *scene, QGraphicsItem * parent, Qt::WindowFlags wFlags)
        : QGraphicsWidget(parent, wFlags)
        , m_scene(scene)
        , m_fillLevel(100)
{
    m_scene->addItem(this);
    m_layout = new QGraphicsLinearLayout(Qt::Horizontal, this);
    setLayout(m_layout);
    layout()->setContentsMargins(20, 20, 20, 20);
    setFlag(QGraphicsItem::ItemIsMovable);
    setAcceptHoverEvents(true);

    m_fillLevelBrush = QBrush(Qt::transparent);
    QLinearGradient bgGradient;
    bgGradient.setCoordinateMode(QGradient::ObjectBoundingMode);
    bgGradient.setStart(0, 0);
    bgGradient.setFinalStop(0, 1);
    bgGradient.setColorAt(0, QColor(Qt::gray).lighter(100));
    bgGradient.setColorAt(1, QColor(Qt::gray).lighter(200));
    m_bgBrush = bgGradient;
    QPalette palette;
    palette.setBrush(QPalette::Background, Qt::transparent);
    setPalette(palette);

    setOpacity(0.7);

    m_blurEffect = new QGraphicsBlurEffect(this);
    m_blurEffect->setProperty("blurRadius", 5);
    setGraphicsEffect(m_blurEffect);
    setGraphicsEffectsEnabled(true);

    m_rotation = new QGraphicsRotation(this);
    m_rotation->setAxis(Qt::YAxis);
    m_rotation->setAngle(0);
    QList<QGraphicsTransform *> txs;
    txs << m_rotation;
    setTransformations(txs);

    QState *blurredState = new QState;
    QState *normalState = new QState;
    m_stateMachine.addState(blurredState);
    m_stateMachine.addState(normalState);
    m_stateMachine.setInitialState(normalState);
    m_makeBlurred = new HoverOutEventTransition(this, QEvent::GraphicsSceneHoverLeave, normalState);
    m_makeBlurred->setTargetState(blurredState);
    m_makeNormal = new QEventTransition(this, QEvent::GraphicsSceneHoverEnter, blurredState);
    m_makeNormal->setTargetState(normalState);

    QParallelAnimationGroup *makeBlurAnimation = new QParallelAnimationGroup(this);
    QParallelAnimationGroup *makeNormalAnimation = new QParallelAnimationGroup(this);

    QPropertyAnimation *blurAnimation = new QPropertyAnimation(m_blurEffect, "blurRadius", makeBlurAnimation);
    blurAnimation->setEndValue(5);
    QPropertyAnimation *transparentizeAnimation = new QPropertyAnimation(this, "opacity", makeBlurAnimation);
    transparentizeAnimation->setEndValue(0.7);
    makeBlurAnimation->addAnimation(blurAnimation);
    makeBlurAnimation->addAnimation(transparentizeAnimation);

    QPropertyAnimation *unblurAnimation = new QPropertyAnimation(m_blurEffect, "blurRadius", makeNormalAnimation);
    unblurAnimation->setEndValue(0);
    QPropertyAnimation *untransparentizeAnimation = new QPropertyAnimation(this, "opacity", makeBlurAnimation);
    untransparentizeAnimation->setEndValue(1.0);
    makeNormalAnimation->addAnimation(unblurAnimation);
    makeNormalAnimation->addAnimation(untransparentizeAnimation);

    m_makeBlurred->addAnimation(makeBlurAnimation);
    m_makeNormal->addAnimation(makeNormalAnimation);
    m_stateMachine.start();
}

void GraphicsToolBar::setGraphicsEffectsEnabled(bool enabled)
{
    m_isGraphicsEffectsEnabled = enabled;
    if (!enabled)
        setGraphicsEffect(0);
    else
        setGraphicsEffect(m_blurEffect);
}

bool GraphicsToolBar::graphicsEffectsEnabled() const
{
    return m_isGraphicsEffectsEnabled;
}

QGraphicsRotation* GraphicsToolBar::rotation() const
{
    return m_rotation;
}

void GraphicsToolBar::addWidget(QWidget *widget)
{
    QGraphicsWidget *gw = m_scene->addWidget(widget);
    m_layout->addItem(gw);
}

void GraphicsToolBar::setBackgroundBrush(QBrush brush)
{
    m_bgBrush = brush;
}

QBrush GraphicsToolBar::backgroundBrush() const
{
    return m_bgBrush;
}

void GraphicsToolBar::setFillLevelBrush(QBrush brush)
{
    m_fillLevelBrush = brush;
}

QBrush GraphicsToolBar::fillLevelBrush() const
{
    return m_fillLevelBrush;
}

void GraphicsToolBar::setFillLevel(int percent)
{
    m_fillLevel = qMax(0, qMin(percent, 100));
}

void GraphicsToolBar::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    painter->save();
    painter->setPen(Qt::NoPen);
    painter->setBrush(m_bgBrush);
    painter->drawRoundedRect(boundingRect(), 10, 10);
    if (m_fillLevel < 100) {
        painter->setClipRect(boundingRect().adjusted(boundingRect().width() * (m_fillLevel / 100.0), 0, 0, 0));
        painter->setBrush(m_fillLevelBrush);
        painter->drawRoundedRect(boundingRect(), 10, 10);
    }
    painter->restore();
}

void GraphicsToolBar::transitionBlurEnabledTo(bool enabled)
{
    if (! graphicsEffectsEnabled())
        return;
    QParallelAnimationGroup *animGroup = new QParallelAnimationGroup(this);
    QPropertyAnimation *blurAnimation = new QPropertyAnimation(m_blurEffect, "blurRadius", animGroup);
    blurAnimation->setStartValue(enabled ? 0 : 5);
    blurAnimation->setEndValue(enabled ? 5 : 0);
    blurAnimation->setDuration(enabled ? 200 : 200);
    QPropertyAnimation *opacityAnimation = new QPropertyAnimation(this, "opacity", animGroup);
    opacityAnimation->setStartValue(enabled ? 1.0 : 0.7);
    opacityAnimation->setEndValue(enabled ? 0.7 : 1.0);
    animGroup->addAnimation(blurAnimation);
    animGroup->addAnimation(opacityAnimation);
    animGroup->start(QAbstractAnimation::DeleteWhenStopped);
}

void GraphicsToolBar::show()
{
    QGraphicsWidget::show();
    setOpacity(1.0);
    m_blurEffect->setProperty("blurRadius", 0);
}

void GraphicsToolBar::hide()
{
    QGraphicsWidget::hide();
}
