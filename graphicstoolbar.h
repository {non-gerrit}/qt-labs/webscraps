/** This file is part of WebScraps **
 *
 * Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).*
 * All rights reserved.

 * Contact:  Nokia Corporation (qt-info@nokia.com)**

 * You may use this file under the terms of the BSD license as follows:
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice, this list
 *      of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice, this
 *      list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution.
 *   3. Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the names of 
 *      its contributors may be used to endorse or promote products derived from this software 
 *      without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef GRAPHICSTOOLBAR_H
#define GRAPHICSTOOLBAR_H

#include <QGraphicsWidget>
#include <QGraphicsLinearLayout>
#include <QLinearGradient>
#include <QGraphicsScene>
#include <QGraphicsBlurEffect>
#include <QGraphicsRotation>
#include <QState>
#include <QStateMachine>
#include <QEventTransition>

class HoverOutEventTransition : public QEventTransition
{
public:
    HoverOutEventTransition(QObject *object, QEvent::Type type, QState *sourceState = 0)
            : QEventTransition(object, type, sourceState) {
    }
protected:
    virtual bool eventTest(QEvent *e);
};

class GraphicsToolBar : public QGraphicsWidget
{
    Q_OBJECT
public:
    GraphicsToolBar(QGraphicsScene *scene, QGraphicsItem * parent = 0, Qt::WindowFlags wFlags = 0);
    void addWidget(QWidget *widget);
    void setBackgroundBrush(QBrush brush);
    QBrush backgroundBrush() const;
    void setFillLevelBrush(QBrush brush);
    QBrush fillLevelBrush() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    bool graphicsEffectsEnabled() const;
    QGraphicsRotation *rotation() const;

public slots:
    void setGraphicsEffectsEnabled(bool enabled);
    void setFillLevel(int percent);
    void show();
    void hide();
    void transitionBlurEnabledTo(bool enabled);

private:
    QGraphicsLinearLayout *m_layout;
    QGraphicsScene *m_scene;
    QBrush m_bgBrush, m_fillLevelBrush;
    bool m_isGraphicsEffectsEnabled;
    QGraphicsEffect *m_blurEffect;
    int m_fillLevel;
    QGraphicsRotation *m_rotation;
    QStateMachine m_stateMachine;
    HoverOutEventTransition *m_makeBlurred;
    QEventTransition *m_makeNormal;
};

#endif // GRAPHICSTOOLBAR_H
