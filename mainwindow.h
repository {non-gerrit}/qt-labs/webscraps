/** This file is part of WebScraps **
 *
 * Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).*
 * All rights reserved.

 * Contact:  Nokia Corporation (qt-info@nokia.com)**

 * You may use this file under the terms of the BSD license as follows:
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice, this list
 *      of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice, this
 *      list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution.
 *   3. Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the names of 
 *      its contributors may be used to endorse or promote products derived from this software 
 *      without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWebView>
#include <QLineEdit>
#include <QProgressBar>
#include <QToolButton>
#include <QList>
#include <QComboBox>
#include <QSettings>
#include <QStateMachine>
#include <QParallelAnimationGroup>
#include "webview.h"
#include "graphicsview.h"
#include "graphicstoolbar.h"
#include "webscrap.h"
#include "addressbar.h"

namespace Ui
{
class MainWindow;
}

class MainWindow : public GraphicsView
{
    Q_OBJECT
    Q_PROPERTY(bool webTitleShown READ webTitleShown WRITE setWebTitleShown)
public:
    enum Movement {
        Enter,
        Leave
    };
    enum RotateDirection {
        Clockwise,
        AntiClockwise
    };
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void initGraphicsView();
    void initBrowser();
    void initScrapBoard();

    void createScrapsToolbar(QGraphicsScene *scene);
    void createBrowserToolbar(QGraphicsScene *scene);
    QAbstractAnimation* createToolbarAnim(GraphicsToolBar *fromToolbar, GraphicsToolBar *toToolbar,
                                          RotateDirection direction, QObject *parent);
    QAbstractAnimation* createScrapsAnim(const Movement move, QObject *parent);
    QAbstractAnimation* createBrowserAnim(const Movement move, QObject *parent);
    void bubbleUpScraps(QList<QGraphicsWidget*> scraps);

    void saveSettings();
    void loadSettings();
    void setWebTitleShown(bool shown);
    bool webTitleShown() const;

    void createAnimations();
    void addScrapToAnimations(WebScrapContainer *scrapContainer);
    void removeScrapFromAnimations(WebScrapContainer *scrapContainer);

public slots:
    void adjustLocation();
    void changeLocation();
    void adjustTitle();
    void addScrap(const QUrl &url, const QSize &pageSize, const QRect &scrapRect, const QPoint &position, qreal xScale = 1.0, qreal yScale = 1.0, int refreshMins = 60);
    void bubbleUpScrap(QGraphicsWidget *scrap = 0);
    void removeSenderScrap();
    void updateDropDownList();
    void scrapDropDownListActivated(int index);
    void searchTextChanged(const QString& text);
    void showBrowserToolbar();
    void showScrapsToolbar();
    void disableScrapSelection();

private:
    Ui::MainWindow *ui;

    QSettings *m_settings;
    QStateMachine m_stateMachine;
    bool m_isWebTitleShown;

    // scraps view
    QComboBox *m_dropDownList;
    QLineEdit *m_searchBar;
    QToolButton *m_searchScrapsButton, *m_gotoBrowserButton;
    GraphicsToolBar *m_scrapsToolbar;
    QList<QGraphicsWidget*> m_scraps;

    // browser view
    WebView *m_webView;
    AddressBar *m_addressBar;
    QToolButton *m_selectScrapsButton, *m_addScrapButton, *m_gotoScrapsButton;
    GraphicsToolBar *m_browserToolbar;

    // animations
    QParallelAnimationGroup *m_browserToScrapsAnimation, *m_scrapsToBrowserAnimation;
    QParallelAnimationGroup *m_scrapsEnter, *m_scrapsLeave;
    QAbstractAnimation *m_toBrowserToolbarAnimation, *m_toScrapsToolbarAnimation;
    QPointF m_scrapsFlyOffset;
};

#endif // MAINWINDOW_H
