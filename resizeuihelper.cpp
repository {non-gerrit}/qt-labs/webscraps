/** This file is part of WebScraps **
 *
 * Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).*
 * All rights reserved.

 * Contact:  Nokia Corporation (qt-info@nokia.com)**

 * You may use this file under the terms of the BSD license as follows:
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice, this list
 *      of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice, this
 *      list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution.
 *   3. Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the names of 
 *      its contributors may be used to endorse or promote products derived from this software 
 *      without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "resizeuihelper.h"
#include <QGraphicsSceneHoverEvent>
#include <QGraphicsSceneMouseEvent>
#include <QHoverEvent>
#include <QMouseEvent>
#include <QEvent>
#include <QGraphicsScene>
#include <QCursor>
#include <QWidget>

ResizeUiHelper::ResizeUiHelper(QGraphicsItem *item, QObject *parent)
        : QObject(parent)
        , m_item(0)
        , m_widget(0)
{
    item->scene()->addItem(this);
    item->installSceneEventFilter(this);
    item->setAcceptHoverEvents(true);
    m_item = item;
    m_rect = item->boundingRect();
    reset();
    setResizeEnabled(true);
}

ResizeUiHelper::ResizeUiHelper(QWidget *widget, QObject *parent)
        : QObject(parent)
        , m_item(0)
        , m_widget(0)
{
    widget->installEventFilter(this);
    widget->setMouseTracking(true);
    m_widget = widget;
    m_rect = widget->geometry();
    reset();
}

void ResizeUiHelper::reset()
{
    m_gripsEnabled.fill(true, int(ResizeUiHelper::Left) + 1);
    m_gripSize = QSizeF(5, 5);
    m_isResizeInProgress = false;
}

void ResizeUiHelper::setRect(const QRectF &rect)
{
    m_rect = rect;
}

QRectF ResizeUiHelper::rect() const
{
    return m_rect;
}

void ResizeUiHelper::setResizeEnabled(bool enabled)
{
    m_isResizeEnabled = enabled;
}

bool ResizeUiHelper::resizeEnabled() const
{
    return m_isResizeEnabled;
}

void ResizeUiHelper::setResizeGripEnabled(GripId grip, bool enabled)
{
    Q_ASSERT(grip <= ResizeUiHelper::Left);
    m_gripsEnabled[int(grip)] = enabled;
}

bool ResizeUiHelper::resizeGripEnabled(GripId grip) const
{
    Q_ASSERT(grip <= ResizeUiHelper::Left);
    return m_gripsEnabled[int(grip)];
}

void ResizeUiHelper::setAllResizeGripsEnabled(bool enabled)
{
    for (int i = int(ResizeUiHelper::TopLeft); i <= int(ResizeUiHelper::Left); i++)
        setResizeGripEnabled(GripId(i), enabled);
}

void ResizeUiHelper::setGripSize(QSizeF sz)
{
    m_gripSize = sz;
}

QSizeF ResizeUiHelper::gripSize() const
{
    return m_gripSize;
}

QRectF ResizeUiHelper::gripRect(GripId grip) const
{
    QPointF pt;
    switch (grip) {
    case ResizeUiHelper::TopLeft:
        pt = m_rect.topLeft(); break;
    case ResizeUiHelper::TopRight:
        pt = m_rect.topRight(); break;
    case ResizeUiHelper::BottomLeft:
        pt = m_rect.bottomLeft(); break;
    case ResizeUiHelper::BottomRight:
        pt = m_rect.bottomRight(); break;
    case ResizeUiHelper::Top:
        pt = QPointF(m_rect.center().x(), m_rect.top()); break;
    case ResizeUiHelper::Right:
        pt = QPointF(m_rect.right(), m_rect.center().y()); break;
    case ResizeUiHelper::Bottom:
        pt = QPointF(m_rect.center().x(), m_rect.bottom()); break;
    case ResizeUiHelper::Left:
        pt = QPointF(m_rect.left(), m_rect.center().y()); break;
    default:
        pt = QPointF();
    }
    QPointF tl = pt - QPointF(m_gripSize.width() / 2, m_gripSize.height() / 2);
    return QRectF(tl, m_gripSize);
}

QCursor ResizeUiHelper::gripCursor(GripId grip) const
{
    switch (grip) {
    case ResizeUiHelper::TopLeft:
    case ResizeUiHelper::BottomRight:
        return Qt::SizeFDiagCursor;
    case ResizeUiHelper::Top:
    case ResizeUiHelper::Bottom:
        return Qt::SizeVerCursor;
    case ResizeUiHelper::TopRight:
    case ResizeUiHelper::BottomLeft:
        return Qt::SizeBDiagCursor;
    case ResizeUiHelper::Right:
    case ResizeUiHelper::Left:
        return Qt::SizeHorCursor;
    default:
        return Qt::ArrowCursor;
    }
}

bool ResizeUiHelper::eventFilter(QObject *watched, QEvent * event)
{
    Q_UNUSED(watched);
    QMouseEvent *mouseEvent = dynamic_cast<QMouseEvent*>(event);
    QPointF eventPos;
    if (mouseEvent)
        eventPos = mouseEvent->pos();
    else
        return false;
    QEvent::Type eventType;
    switch (event->type()) {
    case QEvent::MouseMove:
        if ((mouseEvent->buttons() & Qt::LeftButton) == Qt::LeftButton)
            eventType = QEvent::GraphicsSceneMouseMove;
        else
            eventType = QEvent::GraphicsSceneHoverMove;
        break;
    case QEvent::MouseButtonPress:
        eventType = QEvent::GraphicsSceneMousePress;
        break;
    case QEvent::MouseButtonRelease:
        eventType = QEvent::GraphicsSceneMouseMove;
    default:
        return false;
    }
    return genericEventFilter(eventType, eventPos);
}

bool ResizeUiHelper::sceneEventFilter(QGraphicsItem *watched, QEvent* event)
{
    Q_UNUSED(watched);
    QGraphicsSceneHoverEvent *hoverEvent = dynamic_cast<QGraphicsSceneHoverEvent*>(event);
    QGraphicsSceneMouseEvent *mouseEvent = dynamic_cast<QGraphicsSceneMouseEvent*>(event);
    if (!hoverEvent && !mouseEvent)
        return false;
    QPointF eventPos;
    if (hoverEvent)
        eventPos = hoverEvent->pos();
    if (mouseEvent)
        eventPos = mouseEvent->pos();
    return genericEventFilter(event->type(), eventPos);
}

bool ResizeUiHelper::genericEventFilter(QEvent::Type eventType, QPointF eventPos)
{
    if (!resizeEnabled())
        return false;
    if (eventType == QEvent::GraphicsSceneMouseRelease) {
        m_isResizeInProgress = false;
        return false;
    }
    bool gripSectionMatched = false;
    for (int i = int(ResizeUiHelper::TopLeft); i <= int(ResizeUiHelper::Left); i++) {
        GripId section = GripId(i);
        if (m_isResizeInProgress && eventType == QEvent::GraphicsSceneMouseMove) {
            rectResize(m_buttonDownRect, m_activeResizeGrip, m_buttonDownPos, eventPos);
            return true; // event handled
        }
        if (gripRect(section).contains(eventPos)) {
            if (!resizeGripEnabled(section)) {
                return false;
            }
            if (eventType == QEvent::GraphicsSceneHoverMove) {
                setWatchedCursor(gripCursor(section));
                return true;
            } else if (eventType == QEvent::GraphicsSceneMousePress) {
                m_buttonDownPos = eventPos;
                m_buttonDownRect = m_rect;
                m_activeResizeGrip = section;
                m_isResizeInProgress = true;
                return true;
            }
            gripSectionMatched = true;
        }
    }
    if (!gripSectionMatched && eventType == QEvent::GraphicsSceneHoverMove)
        setWatchedCursor(gripCursor(ResizeUiHelper::NoGrip));
    return false;
}

QRectF ResizeUiHelper::rectResize(QRectF originalRect, GripId activeSection, QPointF originalPoint, QPointF newPoint)
{
    qreal dx1 = 0;
    qreal dy1 = 0;
    qreal dx2 = 0;
    qreal dy2 = 0;
    switch (activeSection) {
    case ResizeUiHelper::Left:
        dx1 = newPoint.x() - originalPoint.x();
        break;
    case ResizeUiHelper::Right:
        dx2 = newPoint.x() - originalPoint.x();
        break;
    case ResizeUiHelper::Top:
        dy1 = newPoint.y() - originalPoint.y();
        break;
    case ResizeUiHelper::Bottom:
        dy2 = newPoint.y() - originalPoint.y();
        break;
    case ResizeUiHelper::TopLeft:
        dy1 = newPoint.y() - originalPoint.y();
        dx1 = newPoint.x() - originalPoint.x();
        break;
    case ResizeUiHelper::BottomRight:
        dy2 = newPoint.y() - originalPoint.y();
        dx2 = newPoint.x() - originalPoint.x();
        break;
    case ResizeUiHelper::TopRight:
        dy1 = newPoint.y() - originalPoint.y();
        dx2 = newPoint.x() - originalPoint.x();
        break;
    case ResizeUiHelper::BottomLeft:
        dy2 = newPoint.y() - originalPoint.y();
        dx1 = newPoint.x() - originalPoint.x();
        break;
    default:
        break;
    }
    QRectF newRect = originalRect.adjusted(dx1, dy1, dx2, dy2);
    if (m_rect != newRect)
        emit rectResized(newRect);
    return (m_rect = newRect);
}

void ResizeUiHelper::setWatchedCursor(QCursor cursor)
{
    if (m_item)
        m_item->setCursor(cursor);
    if (m_widget)
        m_widget->setCursor(cursor);
}

QRectF ResizeUiHelper::boundingRect() const   // dummy
{
    return QRectF();
}

void ResizeUiHelper::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)   // dummy
{
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}


