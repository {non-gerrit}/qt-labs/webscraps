/** This file is part of WebScraps **
 *
 * Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).*
 * All rights reserved.

 * Contact:  Nokia Corporation (qt-info@nokia.com)**

 * You may use this file under the terms of the BSD license as follows:
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice, this list
 *      of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice, this
 *      list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution.
 *   3. Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the names of 
 *      its contributors may be used to endorse or promote products derived from this software 
 *      without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RESIZEUIHELPER_H
#define RESIZEUIHELPER_H

#include <QObject>
#include <QWidget>
#include <QGraphicsItem>
#include <QEvent>

class ResizeUiHelper : public QObject, public QGraphicsItem
{
    Q_OBJECT

public:
    enum GripId {
        NoGrip = -1,
        TopLeft = 0,
        Top,
        TopRight,
        Right,
        BottomRight,
        Bottom,
        BottomLeft,
        Left
    };

    ResizeUiHelper(QGraphicsItem *item, QObject *parent = 0);
    ResizeUiHelper(QWidget *widget, QObject *parent = 0);

    QRectF rect() const;

    void setResizeGripEnabled(GripId grip, bool enabled);
    bool resizeGripEnabled(GripId grip) const;
    void setAllResizeGripsEnabled(bool enabled);

    void setGripSize(QSizeF sz);
    QSizeF gripSize() const;

    virtual QRectF gripRect(GripId grip) const;
    virtual QCursor gripCursor(GripId grip) const;

    bool sceneEventFilter(QGraphicsItem* watched, QEvent* event);
    bool eventFilter(QObject *watched, QEvent * event);
    bool genericEventFilter(QEvent::Type eventType, QPointF eventPos);

    QRectF rectResize(QRectF originalRect, GripId activeGrip, QPointF originalPoint, QPointF newPoint);

    QRectF boundingRect() const; // dummy
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget); // dummy

    void reset();
    bool resizeEnabled() const;

public slots:
    void setResizeEnabled(bool enabled);
    void setRect(const QRectF &rect);

signals:
    void rectResized(const QRectF &rect);

private:
    void setWatchedCursor(QCursor cursor);

    QGraphicsItem *m_item;
    QWidget *m_widget;
    bool m_isResizeEnabled;

    QVector<bool> m_gripsEnabled;
    QRectF m_rect;
    QSizeF m_gripSize;

    QPointF m_buttonDownPos;
    QRectF m_buttonDownRect;
    GripId m_activeResizeGrip;
    bool m_isResizeInProgress;
};

#endif // RESIZEUIHELPER_H
