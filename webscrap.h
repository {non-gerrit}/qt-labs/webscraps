/** This file is part of WebScraps **
 *
 * Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).*
 * All rights reserved.

 * Contact:  Nokia Corporation (qt-info@nokia.com)**

 * You may use this file under the terms of the BSD license as follows:
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice, this list
 *      of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice, this
 *      list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution.
 *   3. Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the names of 
 *      its contributors may be used to endorse or promote products derived from this software 
 *      without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef WEBSCRAP_H
#define WEBSCRAP_H

#include <QWebView>
#include <QGraphicsWebView>
#include <QLinearGradient>
#include <QToolButton>
#include <QGraphicsScale>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>
#include <QPropertyAnimation>
#include "graphicstoolbar.h"
#include "resizeuihelper.h"

class WebScrap : public QGraphicsWebView
{
    Q_OBJECT
public:
    WebScrap(QUrl url, QSize pageSize, QRect scrapRect, QGraphicsItem * parent = 0);
    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* options, QWidget* widget = 0);

    const QTimer *refreshTimer() const;
    void setRefreshInterval(int msecs);
    int refreshInterval() const; // msecs

    QRect scrapRect() const;
    void updateGradient(QRect rect);
    void savePos();
    QPointF savedPos() const;
    bool dimensionsFixed() const;
    bool highlightText(const QString& text);

    void setMouseClicksEnabled(bool enabled);
    bool mouseClicksEnabled() const;

public slots:
    void onLoadFinished();
    void onLoadStarted();
    void onRefresh();
    void setScrapRect(const QRect &rect);
    void openUrlInExternalBrowser(const QUrl &url);
    void setDimensionsFixed(bool fixed);

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    bool event(QEvent *e);

private:
    QRect m_scrapRect;
    int m_refreshInterval;
    QTimer *m_refreshTimer;
    QLinearGradient m_loadingGradient, m_overlayGradient;
    QPointF m_savedPos;
    QPixmap m_webshot;
    bool m_isDimensionsFixed;
    QPixmap m_loadingPix, m_adjustablePix, m_arrowPix, m_scalePix;
    QSize m_pageSize;
    bool m_isLoading, m_isMouseClicksEnabled;
};

class WebScrapContainer : public QGraphicsWidget
{
    Q_OBJECT
public:
    const static int s_padding, s_titlePadding;

    WebScrapContainer(WebScrap *scrap, QGraphicsScene *scene, qreal xScale = 1.0, qreal yScale = 1.0);
    void createToolbar();
    void createEditToolbar();
    void setBoundingRect(QRectF rect);
    QRectF boundingRect() const;
    WebScrap* webScrap() const;
    QGraphicsDropShadowEffect *glow() const;
    void updateGlow();
    const QGraphicsScale *scrapScale() const;
    void setEnterAnimation(QPropertyAnimation *animation);
    QPropertyAnimation* enterAnimation() const;
    void setLeaveAnimation(QPropertyAnimation *animation);
    QPropertyAnimation *leaveAnimation() const;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* options, QWidget* widget = 0);

public slots:
    void setTitle(const QString& title);
    void handleCloseButtonClicked();
    void handleRectResized(QRectF rect);
    void setLocated(bool located);
    void setSearchMatched(bool matched);
    void handleEditButtonClicked(bool checked);
    void handleResetZoomClicked();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

signals:
    void frameClicked();
    void removeSelf();

private:
    QGraphicsScene *m_scene;
    WebScrap *m_scrap;
    GraphicsToolBar *m_toolbar;
    QToolButton *m_closeButton, *m_adjustButton, *m_reloadButton;
    GraphicsToolBar *m_editToolbar;
    QLineEdit *m_refreshIntervalLineEdit;
    QComboBox *m_refreshUnitComboBox;
    QPushButton *m_resetZoomPushButton;
    ResizeUiHelper *m_resizer;
    QGraphicsScale *m_scrapScale;
    QGraphicsDropShadowEffect *m_glow;
    bool m_located, m_searchMatched;
    QPropertyAnimation *m_enterAnimation, *m_leaveAnimation;
    QString m_title;
    QBrush m_titleBrush, m_backgroundBrush;
    QRectF m_boundingRect;
};

Q_DECLARE_METATYPE(WebScrapContainer*)
#endif // WEBSCRAP_H
