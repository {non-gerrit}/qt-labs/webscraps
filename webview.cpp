/** This file is part of WebScraps **
 *
 * Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).*
 * All rights reserved.

 * Contact:  Nokia Corporation (qt-info@nokia.com)**

 * You may use this file under the terms of the BSD license as follows:
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice, this list
 *      of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice, this
 *      list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution.
 *   3. Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the names of 
 *      its contributors may be used to endorse or promote products derived from this software 
 *      without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "webview.h"
#include <QMouseEvent>
#include <QWebFrame>
#include <QWebHitTestResult>
#include <QWebElement>
#include <QLinearGradient>
#include <QPushButton>
#include <QStyleOptionGraphicsItem>
#include "resizeuihelper.h"

WebView::WebView(QGraphicsScene *scene, QGraphicsItem * parent)
        : QGraphicsWebView(parent)
        , m_scene(scene)
        , m_scrapSelectionEnabled(false)
        , m_scrapRect(QRect())
        , m_scrapSelected(false)
{
    scene->addItem(this);
    setAcceptHoverEvents(true);
    createAddScrapToolbar();
    m_resizer = new ResizeUiHelper(this, this);
    m_resizer->setResizeEnabled(false);
    connect(m_resizer, SIGNAL(rectResized(QRectF)), SLOT(setScrolledScrapRect(QRectF)));
}

void WebView::createAddScrapToolbar()
{
    m_addScrapToolbar = new GraphicsToolBar(m_scene);
    m_cancelButton = new QPushButton(tr("Cancel"));
    m_addButton = new QPushButton(tr("Add"));
    m_addScrapToolbar->addWidget(m_cancelButton);
    m_addScrapToolbar->addWidget(m_addButton);
    m_addScrapToolbar->setPos(0, 0);
    m_addScrapToolbar->setZValue(1001);
    m_addScrapToolbar->setBackgroundBrush(QBrush(QColor(10, 10, 100, 50)));
    m_addScrapToolbar->setGraphicsEffectsEnabled(false);
    m_addScrapToolbar->layout()->setContentsMargins(10, 10, 10, 10);
    m_addScrapToolbar->setVisible(false);
    connect(m_cancelButton, SIGNAL(clicked()), SLOT(unselectScrap()));
    connect(m_addButton, SIGNAL(clicked()), SLOT(addScrap()));
}

void WebView::showAddScrapToolbar()
{
    if (!m_scrapRect.isValid())
        return;
    m_addScrapToolbar->setPos(m_scrapRect.translated(-page()->mainFrame()->scrollPosition()).topRight());
    m_addScrapToolbar->moveBy(-m_addScrapToolbar->rect().width(), -m_addScrapToolbar->rect().height());
    if (!m_scene->sceneRect().contains(m_addScrapToolbar->mapToScene(m_addScrapToolbar->rect().topRight()))) {
        m_addScrapToolbar->setPos(0, 0);
    }
    m_addScrapToolbar->setVisible(true);
}

void WebView::hideAddScrapToolbar()
{
    m_addScrapToolbar->setVisible(false);
}

void WebView::setScrapRect(const QRect &rect)
{
    QRect updateRect = m_scrapRect.united(rect);
    m_scrapRect = rect;
    update(updateRect.translated(-page()->mainFrame()->scrollPosition()));
}

void WebView::setScrolledScrapRect(const QRectF &rect)
{
    setScrapRect(rect.translated(page()->mainFrame()->scrollPosition()).toRect());
}

void WebView::selectScrap()
{
    m_scrapSelected = true;
    showAddScrapToolbar();
    m_resizer->setRect(m_scrapRect.translated(-page()->mainFrame()->scrollPosition()));
    m_resizer->setResizeEnabled(true);
    page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal, Qt::ScrollBarAlwaysOff);
    page()->mainFrame()->setScrollBarPolicy(Qt::Vertical, Qt::ScrollBarAlwaysOff);
}

void WebView::unselectScrap()
{
    m_scrapSelected = false;
    hideAddScrapToolbar();
    m_resizer->setResizeEnabled(false);
    update();
    page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal, Qt::ScrollBarAsNeeded);
    page()->mainFrame()->setScrollBarPolicy(Qt::Vertical, Qt::ScrollBarAsNeeded);
}

void WebView::addScrap()
{
    if (m_scrapSelectionEnabled && m_scrapSelected && m_scrapRect.isValid())
        emit scrapAdded(url(), page()->viewportSize(), m_scrapRect, m_scrapRect.translated(-page()->mainFrame()->scrollPosition()).topLeft());
    m_scrapSelected = false;
    hideAddScrapToolbar();
}

void WebView::setScrapSelectionEnabled(bool enabled)
{
    if (m_scrapSelectionEnabled == enabled)
        return;
    m_scrapSelectionEnabled = enabled;
    if (!enabled)
        unselectScrap();
}

bool WebView::scrapSelectionEnabled() const
{
    return m_scrapSelectionEnabled;
}

void WebView::hoverMoveEvent(QGraphicsSceneHoverEvent* event)
{
    if (!m_scrapSelectionEnabled || m_scrapSelected) {
        QGraphicsWebView::hoverMoveEvent(event);
        return;
    }
    QWebFrame* mainFrame = page()->mainFrame();
    if (mainFrame != page()->frameAt(event->pos().toPoint()) ||
            mainFrame->scrollBarGeometry(Qt::Horizontal).contains(event->pos().toPoint()) ||
            mainFrame->scrollBarGeometry(Qt::Vertical).contains(event->pos().toPoint())) {
        m_scrapRect = QRect();
        QGraphicsWebView::hoverMoveEvent(event);
        return;
    }
    QWebElement hitElement = mainFrame->hitTestContent(event->pos().toPoint()).element();
    QRect hitRect = hitElement.geometry();
    if ((event->modifiers() & Qt::ControlModifier) != Qt::ControlModifier) {
        // So we can use Ctrl+Hover to find small items
        while (hitRect.height() < 100 || hitRect.width() < 100) {
            if (hitElement.isNull() || hitElement == mainFrame->documentElement())
                break;
            hitElement = hitElement.parent();
            hitRect = hitElement.geometry();
        }
    }
    if (hitRect.translated(-mainFrame->scrollPosition()).contains(event->pos().toPoint())) {
        m_scrapRect = hitRect;
    } else {
        m_scrapRect = QRect();
    }
    QGraphicsWebView::hoverMoveEvent(event);
    update();
}

void WebView::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (!m_scrapSelectionEnabled) {
        QGraphicsWebView::mousePressEvent(event);
        return;
    }
    if (page()->mainFrame()->scrollBarGeometry(Qt::Horizontal).contains(event->pos().toPoint()) ||
            page()->mainFrame()->scrollBarGeometry(Qt::Vertical).contains(event->pos().toPoint())) {
        QGraphicsWebView::mousePressEvent(event);
        return;
    }
    if (m_scrapRect.isValid()) {
        if (!m_scrapSelected)
            selectScrap();
    } else {
        QGraphicsWebView::mousePressEvent(event);
        return;
    }
}

void WebView::keyPressEvent(QKeyEvent *event)
{
    if (m_scrapSelectionEnabled && event->key() == Qt::Key_Escape) {
        unselectScrap();
        return;
    }
    QGraphicsWebView::keyPressEvent(event);
}

void WebView::paint(QPainter* painter, const QStyleOptionGraphicsItem* options, QWidget* widget)
{
    painter->fillRect(options->rect, Qt::white);
    QGraphicsWebView::paint(painter, options, widget);
    if (!m_scrapSelectionEnabled)
        return;
    QPainterPath pageAreaPath, greyedoutPath;
    QPainterPath scrollbars;
    if (page() && page()->mainFrame()) {
        pageAreaPath.addRect(page()->mainFrame()->geometry());
        scrollbars.addRect(page()->mainFrame()->scrollBarGeometry(Qt::Horizontal));
        scrollbars.addRect(page()->mainFrame()->scrollBarGeometry(Qt::Vertical));
        pageAreaPath -= scrollbars;
    }
    greyedoutPath = pageAreaPath;
    if (m_scrapRect.isValid()) {
        QPainterPath scrapPath;
        QRect scrapRect = m_scrapRect.translated(-page()->mainFrame()->scrollPosition());
        scrapPath.addRect(scrapRect);
        greyedoutPath -= scrapPath;
    }
    painter->setClipPath(greyedoutPath);
    painter->setPen(Qt::NoPen);
    painter->setBrush(QBrush(Qt::black));
    painter->setOpacity(0.5);
    painter->drawRect(rect());
    if (m_scrapRect.isValid()) {
        QRect scrapRect = m_scrapRect.translated(-page()->mainFrame()->scrollPosition());
        painter->setClipPath(pageAreaPath);
        painter->setOpacity(1);
        painter->setPen(QPen(Qt::white, 2));
        painter->setBrush(Qt::NoBrush);
        painter->drawRect(scrapRect);
        if (m_scrapSelected) {
            QList<QPoint> nodes;
            nodes << scrapRect.topLeft() << scrapRect.topRight();
            nodes << scrapRect.bottomLeft() << scrapRect.bottomRight();
            nodes << QPoint(scrapRect.center().x(), scrapRect.top());
            nodes << QPoint(scrapRect.center().x(), scrapRect.bottom());
            nodes << QPoint(scrapRect.left(), scrapRect.center().y());
            nodes << QPoint(scrapRect.right(), scrapRect.center().y());
            foreach(QPoint node, nodes) {
                QLinearGradient g(node - QPointF(5, 5), node + QPointF(5, 5));
                g.setColorAt(0, Qt::white);
                g.setColorAt(0.9, Qt::black);
                painter->setBrush(g);
                painter->setPen(Qt::NoPen);
                painter->drawEllipse(node, 5, 5);
            }
        }
    }
}

void WebView::show()
{
    QGraphicsWebView::show();
}

void WebView::hide()
{
    QGraphicsWebView::hide();
}

void WebView::enableCachedMode()
{
    setCacheMode(QGraphicsItem::DeviceCoordinateCache, QSize(1000, 1000));
}

void WebView::disableCachedMode()
{
    setCacheMode(QGraphicsItem::NoCache);
}
