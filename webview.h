/** This file is part of WebScraps **
 *
 * Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).*
 * All rights reserved.

 * Contact:  Nokia Corporation (qt-info@nokia.com)**

 * You may use this file under the terms of the BSD license as follows:
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice, this list
 *      of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice, this
 *      list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution.
 *   3. Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the names of 
 *      its contributors may be used to endorse or promote products derived from this software 
 *      without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY 
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef WEBVIEW_H
#define WEBVIEW_H

#include <QGraphicsWebView>
#include <QGraphicsSceneMouseEvent>
#include <QPushButton>
#include <QKeyEvent>
#include <QPainter>
#include <QWebPage>
#include "graphicstoolbar.h"
#include "resizeuihelper.h"

class WebView : public QGraphicsWebView
{
    Q_OBJECT
public:
    WebView(QGraphicsScene *scene, QGraphicsItem * parent = 0);
    bool scrapSelectionEnabled() const;
    void createAddScrapToolbar();
    void showAddScrapToolbar();
    void hideAddScrapToolbar();

public slots:
    void setScrapSelectionEnabled(bool enabled);
    void selectScrap();
    void unselectScrap();
    void addScrap();
    void show();
    void hide();
    void setScrapRect(const QRect &rect);
    void setScrolledScrapRect(const QRectF &rect);
    void enableCachedMode();
    void disableCachedMode();

signals:
    void scrapAdded(const QUrl &url, const QSize &pageSize, const QRect &scrapRect, const QPoint &position);

protected:
    void hoverMoveEvent(QGraphicsSceneHoverEvent* event);
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void keyPressEvent(QKeyEvent *event);
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* options, QWidget* widget = 0);

private:
    QGraphicsScene *m_scene;
    bool m_scrapSelectionEnabled;
    QRect m_scrapRect;
    bool m_scrapSelected;

    GraphicsToolBar *m_addScrapToolbar;
    QPushButton *m_addButton, *m_cancelButton;
    ResizeUiHelper *m_resizer;
};

#endif // WEBVIEW_H
